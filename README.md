This repository is a part of the interdisciplinary salmon lice identification project at the University of Bergen

Questions or comments to the project can be directed to: erik.hanson@uib.no

See documentation.txt for more info