function im_struct = cropLiceStruct(im_struct,crop_radi)
% im_struct = cropLiceStruct(im_struct,crop_radi) laster inn en
% bilde-struct og legger til et element med cropet bilde. 


t = .1; % treshold for initial binary segmentation (as a fraction of the OTSU-value)


%går igjennom hele structen
for i = 1:length(im_struct)
    im_struct(i).crop_radi = crop_radi;
    
    I = im_struct(i).original; % laster et enkelt bilde
I = im2double(I); %gjør bilde om til flyttall skalert mellom 0 og 1
I = rgb2gray(I);% tar vekk farger (her mister jeg mye info, men alt blir mye enklere)

T = graythresh(I)*t; % lager binært bilde med en automatisk terklingsverdi (multiplisert med t)
BW = (I<T);

im_struct(i).T = T;

%finner den største prikken
BW  = bwareafilt(BW,[1 50]);

CC = bwconncomp(BW);

for j = 1:length(CC.PixelIdxList) 
cc_size(j) = length(CC.PixelIdxList{j});
end
[~,largest] = max(cc_size);

BW2 = zeros(size(BW));
BW2(CC.PixelIdxList{largest}) = 1;

BW2 = bwconvhull(BW2);
BW2 = bwmorph(BW2,'shrink',Inf); 


%croper bildet til en sirkel med gitt radius
BW3 = CropCircleCenter(BW2,im_struct(i).crop_radi); 

im_struct(i).center = BW2;
im_struct(i).mask = BW3;


[a,b] = find(BW3);
I_masked = I.*BW3;
im_struct(i).crop = I_masked(min(a):max(a),min(b):max(b));

clear cc_size
end
