function out = liceID_2(options)
% out = liceID_2(options) Lice identification model based on the method
%from Hanson et al. 2021. All settings are specified in a designated
%options file. The path to this file is the only argument in this function.
% The lice identification result is written to file. The parameter out will
% eigther be 'true' (identification mode) or an an array containing the number
% of correct identifications and numebr of total known IDs (test mode)

%For further documentation, see documentation.txt

%EAH, May 2021


%try

% extract date and time informatrion 
dat = char(datetime,'yyyy-MM-dd-hhmm');

%Load the options file
fid = fopen(options);
C = textscan(fid, '%s','delimiter','\n');
fclose(fid);

file_cont = C{1};
%execute the options commands
for i = 1:length(file_cont)
    eval([ file_cont{i} ';' ] )
end

%reading image data
im_struct_ref = createLiceStructFromFolder(ref_folder); %establish a struct with info from the image files
im_struct_ref = cropLiceStruct(im_struct_ref,crop_radius); %adding croped image to he scruct
%continiue reading the unknown images 
im_struct_toID = createLiceStructFromFolder(targ_folder);
im_struct_toID = cropLiceStruct(im_struct_toID,crop_radius);


%% Matching images using double for loop
n_toID = length(im_struct_toID);
n_ref = length(im_struct_ref);

%running loop in paralell. This may be a problem on some sytems. In these
%cases the loop should be changed to a for-loop. A switch should be
%included in a future version

parfor j = 1:n_toID
    
    for i = 1:n_ref
        im1 = im_struct_toID(j).crop; % extracting croped image
        %  matcher the image histograms
        hi = imhist(im1); 
        im2 = histeq(im_struct_ref(i).crop,hi);
        
        %performing the registration
        im_struct_toID(j).reg(i) = RegisterPhasecorrection(im1,im2);
    end
    
end

%% establishing similarity metric

for i = 1:length(im_struct_toID) % for each of the images with unknown ID
    
    def = zeros([1, length(im_struct_ref)]);

    for j = 1:length(im_struct_ref)
    %extracting the size of the deformation field for each of the reference
    %images
    def(j) = norm(sqrt(im_struct_toID(i).reg(j).DisplacementField(:,:,1).^2+im_struct_toID(i).reg(j).DisplacementField(:,:,2).^2   ) );

    end
     
 %sotrting the scores and assigning ID to unknown images
 score = def;
 im_struct_toID(i).score = score;
 [~,id] = min(im_struct_toID(i).score);
 [~,score_sort] = sort(score);

 im_struct_toID(i).IDlist = score_sort;

 im_struct_toID(i).IDed_as = im_struct_ref(id).IDnr;
 %assigning 3 alternative IDs
 im_struct_toID(i).IDed_as_alt = im_struct_ref(score_sort(2)).IDnr;
 im_struct_toID(i).IDed_as_alt2 = im_struct_ref(score_sort(3)).IDnr;
 im_struct_toID(i).IDed_as_alt3 = im_struct_ref(score_sort(4)).IDnr;

end

%Evaluation mode. 
if evaluation_mode 

%counting number of correct IDs and number of known IDs
antall = 0;
antall_kjent = 0;
 for i =1:length(im_struct_toID)
    
      if strcmp(im_struct_toID(i).IDnr,im_struct_toID(i).IDed_as)  || strcmp(im_struct_toID(i).IDnr,im_struct_toID(i).IDed_as_alt) ||  strcmp(im_struct_toID(i).IDnr,im_struct_toID(i).IDed_as_alt2) ||  strcmp(im_struct_toID(i).IDnr,im_struct_toID(i).IDed_as_alt3)
          antall = antall +1;
      end
      
      if im_struct_toID(i).knownID
          antall_kjent = antall_kjent +1;
      end
      
 end
 
 %generating evaluation report
     str =['Lice ID test report - ' dat '\n \n'...
         'Identifying lice from: '  im_struct_toID(1).experiment '\n'...
         'Referece images: ' im_struct_ref(1).experiment '\n' '\n'...
         'Cropping radius: ' num2str(im_struct_toID(1).crop_radi) '\n \n'...
         'Correct classification: ' num2str(antall) ' of ' num2str(antall_kjent) '\n' ];
     
 
 %writing report to file    
 fileID = fopen([output_name '_test_report_' dat '.txt'],'w');
 fprintf(fileID,str);
 fclose(fileID);
 %returning number of correct IDs and total number of known IDs
 out = [antall antall_kjent];
 
end %of evaluation mode
 
%formating results as a table
T = struct2table(im_struct_toID);

%extracting relevant columns
T2 = T(:,[1 16:19]);
T2.Properties.VariableNames = {'File name' 'Estimated ID' 'Alternative ID 1' 'Alternative ID 2', 'Alternative ID 3'};

%writing results to file
writetable(T2,[output_name '.xls'])

if ~evaluation_mode
out = true;
end

%catch
%    out = false;
%    return
%end

