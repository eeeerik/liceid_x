function [MOVINGREG] = RegisterPhasecorrection(MOVING,FIXED)
%[MOVINGREG] = RegisterPhasecorrection(MOVING,FIXED)
% performing sequential image registration (linear/nonlinear)
% using the matlab buildt-in image registration tools.


% Default spatial referencing objects
fixedRefObj = imref2d(size(FIXED));
movingRefObj = imref2d(size(MOVING));

% Phase correlation
tform = imregcorr(MOVING,movingRefObj,FIXED,fixedRefObj,'transformtype','similarity','Window',true);
MOVINGREG.Transformation = tform;
MOVINGREG.RegisteredImage = imwarp(MOVING, movingRefObj, tform, 'OutputView', fixedRefObj, 'SmoothEdges', true);

% Nonrigid registration
[MOVINGREG.DisplacementField,MOVINGREG.RegisteredImage] = imregdemons(MOVINGREG.RegisteredImage,FIXED,100,'AccumulatedFieldSmoothing',1,'PyramidLevels',3,'DisplayWaitbar',false);

% Store spatial referencing object
MOVINGREG.SpatialRefObj = fixedRefObj;

end

