function im_struct = createLiceStructFromFolder(pathfolder)
% im_struct = createLiceStructFromFolder(pathfolder) tar inn en sti til en
% katalog med bilder 'pathfolder' og returnerer en struct 'im_struct' med informasjon om
% bildene.

%koden er ikke generell og vil bare fungere med navngivningskonvensjonen
%som Adele innførte høsten 2020

img_extention = 'png';

%leser liste av filer i katalogen
fileinfo = dir([pathfolder '/*.' img_extention]);
filenames = {fileinfo.name};

%etsablish image struct
for i = 1:length(filenames)
    
    %legger inn filnavn
    im_struct(i).File_name = filenames{i};

%laster inn de originale bildene    
im_struct(i).original = imread([pathfolder '/' filenames{i}]);

%laster inn navne på eksperimentet (fra navnet på katalogen filene ligger i)
im_struct(i).experiment = fliplr(extractBefore(fliplr(fileinfo(i).folder),'/'));

%under lasters de ulike deler av informasjon som ligger i selve fil-navnet

%extract IDnumber (starts with 000 if ID is unknown)
im_struct(i).IDnr = extractBefore(filenames{i},"_");

%set ID-status
if sum(filenames{i}(1:3)=='000') ==3
im_struct(i).knownID = 0;
else
im_struct(i).knownID = 1;
end

%extract "kar" 
im_struct(i).kar = extractBetween(filenames{i},"_","_");

%extract date

tempstr = extractAfter(filenames{i},"_");
im_struct(i).date = extractAfter(tempstr,"_");

end


