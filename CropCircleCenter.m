function mask = CropCircleCenter(BW,r)
%mask = CropCircleCenter(BW); creats a circular mask with radi r and 
%center in the binary image BW
%
BW = bwconvhull(BW);
BW = bwmorph(BW,'shrink',Inf); 


dis = bwdist(BW);
mask = dis<r;

